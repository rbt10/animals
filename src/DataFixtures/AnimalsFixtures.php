<?php

namespace App\DataFixtures;

use App\Entity\Animals;
use App\Entity\Continent;
use App\Entity\Famille;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AnimalsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /*
        * création des continents
        */

        $europe = new Continent();
        $europe->setLibelle("Europe");
        $manager->persist($europe);

        $afrique = new Continent();
        $afrique->setLibelle("Afrique");
        $manager->persist($afrique);

        $amerique = new Continent();
        $amerique->setLibelle("Amérique");
        $manager->persist($amerique);

        $asie = new Continent();
        $asie->setLibelle("Asie");
        $manager->persist($asie);

        $oceanie = new Continent();
        $oceanie->setLibelle("Océanie");
        $manager->persist($oceanie);


        /*
         * insertion des familles des animaux afin de remplir la base de données
         */

        $f1 = new Famille();
        $f1->setLibelle("les mammifères")
            ->setText("mange bien")
            ->setDescription("animaux vertébrés nourrissant leurs petits avec du lait");

        $manager->persist($f1);

        $f2 = new Famille();
        $f2->setLibelle("reptiles")
            ->setText("mange beaucoup")
            ->setDescription("animaux vertébrés qui rampent");

        $manager->persist($f2);

        $f3 = new Famille();
        $f3->setLibelle("poissons")
            ->setText("mange bien")
            ->setDescription("animaux invertébrés du monde aquatique");

        $manager->persist($f3);

        /*
         *insertion des animaux afin de remplir la base de données
         */

        $a1 = new Animals();
        $a1->setNom("Chien")
            ->setDescription("un animal domestique")
            ->setImage("chien.jfif")
            ->setPoids(10)
            ->setDangereux(false)
            ->setFamille($f1)
            ->addContinent($amerique)
            ->addContinent($afrique)
            ->addContinent($asie)
            ->addContinent($europe)
            ->addContinent($oceanie)
        ;

        //préparer la requete
        $manager->persist($a1);

        $a2= new Animals();
        $a2->setNom("lion")
            ->setDescription("le roi de la jungle, animal ")
            ->setImage("lion.jfif")
            ->setPoids(100)
            ->setDangereux(true)
            ->setFamille($f1)
            ->addContinent($amerique)
            ->addContinent($afrique)
            ->addContinent($asie)
        ;

        //préparer la requete
        $manager->persist($a2);

        $a3 = new Animals();
        $a3->setNom("Baleine")
            ->setDescription("l'un de plus gros et gand animal")
            ->setImage("baleine.jfif")
            ->setPoids(300)
            ->setDangereux(false)
            ->setFamille($f1)
            ->addContinent($amerique)
            ->addContinent($oceanie)

        ;


        //préparer la requete
        $manager->persist($a3);

        $a4 = new Animals();
        $a4->setNom("Crocodile")
            ->setDescription("un animal d'élevage, mamifère qui donne beaucoup de lait")
            ->setImage("crocodile.jfif")
            ->setPoids(100)
            ->setDangereux(false)
            ->setFamille($f2)
            ->addContinent($amerique)
            ->addContinent($afrique)
            ->addContinent($asie)
        ;
        //préparer la requete

        $manager->persist($a4);

        $a6 = new Animals();
        $a6->setNom("Tilapia")
            ->setDescription("un animal se trouvant en Afrique")
            ->setImage("tilapia.jfif")
            ->setPoids(70)
            ->setDangereux(false)
            ->setFamille($f3)
            ->addContinent($amerique)
            ->addContinent($afrique)
            ->addContinent($asie)
        ;
        //préparer la requete

        $manager->persist($a6);

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
