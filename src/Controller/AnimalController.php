<?php

namespace App\Controller;

use App\Entity\Animals;
use App\Repository\AnimalsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnimalController extends AbstractController
{
    #[Route('/', name: 'animaux')]
    public function index(AnimalsRepository $repository): Response
    {
        $animaux = $repository->findAll();
        return $this->render('animal/index.html.twig',[
            "animaux" =>$animaux
        ]);
    }

    #[Route('/animal/{id}', name: 'afficher_animal')]
    public function afficherAnimal(Animals $animal): Response
    {
        return $this->render('animal/afficherAnimal.html.twig',[
            "animal" => $animal
        ]);
    }
}
